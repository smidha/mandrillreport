package com.arches.utilities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;


/**
 *
 * Program to generate UUID and return it in String format
 */
public class UUIDGenerator{
    private String uuid;
 
    private UUIDGenerator() throws IllegalAccessException
    {
        throw new IllegalAccessException();
    }
       public static String getUUID()
       {
           UUID id=UUID.randomUUID();
           return id.toString();
       }
	
    
}
