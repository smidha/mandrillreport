package com.arches.utilities;

import java.awt.List;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Formatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.arches.mandrillreport.MandrillReport;
import com.arches.mandrillreport.MandrillReport.Event;
import com.arches.mandrillreport.MandrillReport.Message;
import com.arches.mandrillreport.MandrillReport.Smtp_Events;
import com.arches.mandrillreport.MandrillReport.SyncEvents;
import com.arches.mandrillreport.MandrillReport.User_Agent_Parsed;

public class DBConfiguration {
	/*
	 * To change this license header, choose License Headers in Project Properties.
	 * To change this template file, choose Tools | Templates
	 * and open the template in the editor.
	 */

	    //declare class variables
	    public static String driver;
	    public static String url;
	    public static String username;
	    public static String password;
	    public static String DB;
	    public static String otherEventsTable;
	    public static String syncEventsTable;
	    public static String openTable;
	    public static String clickTable;
	    public static String emailInstanceTable;//send table
	    public static String emailTypeTable;
	    public static String DBfile;
	    public static String tenantuid;
	  //  public static String ranexaFulfillmentTable;
	   // public static String ranexaReponsysTable;
	   // public static String ranexaUsername;
	   // public static String ranexaPassword;
	   // public static String ranexaDB;
	   // public static String ranexaUrl;
	    //public static String ranexaTag;
	    
	    public static enum mandrillEventName {SEND,OPEN,CLICK,SOFT_BOUNCE,HARD_BOUNCE,REJECT};
	       
	    
	        public static void configureDB(String DBf)
	        {
	            //initialize variables and get name of DB Credentials file
	            DBfile=DBf;
	        try{
	            ResourceBundle DBResource = ResourceBundle.getBundle(DBfile);
	            url=DBResource.getString("url");
	            username=DBResource.getString("username");
	            password=DBResource.getString("password");
	            DB=DBResource.getString("DB");
	            otherEventsTable=DBResource.getString("otherEventsTable");
	            syncEventsTable=DBResource.getString("syncEventsTable");
	            openTable=DBResource.getString("openTable");
	            clickTable=DBResource.getString("clickTable");
	            emailInstanceTable=DBResource.getString("emailInstanceTable");
	            emailTypeTable=DBResource.getString("emailTypeTable");
	            driver=DBResource.getString("driver");
	      //      ranexaDB=DBResource.getString("ranexaDB");
	       //     ranexaUrl=DBResource.getString("ranexaUrl");
	        //    ranexaFulfillmentTable=DBResource.getString("ranexaFulfillmentTable");
	         //   ranexaReponsysTable=DBResource.getString("ranexaResponsysTable");
	  //         ranexaTag=DBResource.getString("ranexaTag");
	   //        ranexaUsername=DBResource.getString("ranexaUsername");
	    //       ranexaPassword=DBResource.getString("ranexaPassword");
	           DateTimeZone.setDefault(DateTimeZone.UTC);//joda time:for open and click events
	           
	             }
	         catch(Exception e)
	            {
	                System.out.println("Error in configuring DB credentials."+e);
	                 Logger.getLogger(DBConfiguration.class.getName()).log(Level.SEVERE, null, e);
	                 MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
	            }
	        }
	        public static void addEvents(Event event)
	        {
	        	System.out.println("Adding to db...Event: " + event.event);
	    		System.out.println("Receiver Email: " + event.msg.email);
	    		System.out.println("Msg sender: " + event.msg.sender);
	    		
	    	
	    		
	    		 try 
	    		 {
					Class.forName(DBConfiguration.driver);
				 }
	    		 catch (ClassNotFoundException e)
	    		 {
					// TODO Auto-generated catch block
					e.printStackTrace();
					 MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
				 }
	 	        //connect to DB
	            Connection conn = null;
	        	try {
					conn = DriverManager.getConnection(DBConfiguration.url,DBConfiguration.username,DBConfiguration.password);
					String emailUUID=lookup_EmailType(event,conn);   
				
					if(event.event==null)
					{
						return;
					}
					if(event.msg!=null && event.msg.smtp_events==null)
					{
						//for later use in open/click events
						event.msg.smtp_events=new Smtp_Events[1];
						event.msg.smtp_events[0]=new Smtp_Events();
					}
					
					if(((event.event).toUpperCase().trim()).equals(mandrillEventName.SEND.toString()) ||  ((event.event).toUpperCase().trim()).equals(mandrillEventName.REJECT.toString()))
					{
						addSendEvent(emailUUID,event,conn);
					}
					
					else if(((event.event).toUpperCase().trim()).equals(mandrillEventName.OPEN.toString()))
					{
						addOpenEvent(emailUUID,event,conn);
					}
					else if(((event.event).toUpperCase().trim()).equals(mandrillEventName.CLICK.toString()))
					{
						addClickEvent(emailUUID,event,conn);
					}
					else
					{
						addOtherEvent(emailUUID,event,conn);
					}
					
				
	        	} 
				catch (SQLException e)
				{
					System.out.println("SQL Exception in adding data to mandrill events table");
					// TODO Auto-generated catch block
					e.printStackTrace();
					 MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
				
				}
	               finally
	               {
	            	   try {
	            		   
	            		   if(conn!=null)
	            			   {
	            			   	
	            			   		conn.close();
	            			   		System.out.println("closing connection");
	            			   }
					       } 
	            	   catch (SQLException e) 
	            	   {
						// TODO Auto-generated catch block
						e.printStackTrace();
						 MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
					    }
	               }
	               
	        	System.out.println("not calling ranexa specific code");
	    		/*This code will not handle ranexa table data. This is for all mandrill events
	    		 * ranexamandrillreport code will add data to ranexa specific tables
	    		 //handle ranexa events
	    		 
				
	        		try 
	        		{
						handleRanexaEvents(event);
					}
	        		catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
					}
					*/
	        }
	        
	        
	        public static String lookup_EmailType(Event event,Connection conn) throws SQLException
	        {
	        	String query_withTemplate="select emailUUID from "+DBConfiguration.emailTypeTable+" where msg_subject=? AND msg_sender=? AND msg_template=?";
	        	String query_withoutTemplate="select emailUUID from "+DBConfiguration.emailTypeTable+" where msg_subject=? AND msg_sender=?";
	        	String uuid=null;
	        	PreparedStatement ps=null;
	        	ResultSet rs=null;
	        	
	        try
	        {
	        	if(event.msg.template==null || event.msg.template.equals(""))
	        	{
	        		System.out.println("template is null");
	        		ps=conn.prepareStatement(query_withoutTemplate);
	        		ps.setString(1,event.msg.subject);
	        		ps.setString(2,event.msg.sender);
	        		
	        	}
	        	else
	        	{ System.out.println("template is not null");
	        		ps=conn.prepareStatement(query_withTemplate);
	        		ps.setString(1,event.msg.subject);
	        		ps.setString(2,event.msg.sender);
	        		ps.setString(3,event.msg.template);
	        	}
	            
	        	 rs=ps.executeQuery();
	        	if(rs.next())
	        	{
	        		//data found
	        		System.out.println("email type exists..get uuid");
	        	    uuid=rs.getString("emailUUID").trim();
	        		
	        	}
	        	else
	        	{//no data found
	        		System.out.println("email type does not exist..create uuid and add to db");
	        		String temp_uuid=UUIDGenerator.getUUID();
	                BigDecimal bd_currentTS=new BigDecimal(System.currentTimeMillis());
	     		   
	        		PreparedStatement ps_addEmailType=conn.prepareStatement("insert into "+DBConfiguration.emailTypeTable+" values(?,?,?,?,?)");
	        		
	        		ps_addEmailType.setString(1,temp_uuid.trim());
	        		ps_addEmailType.setString(2,event.msg.subject.trim());
	        		ps_addEmailType.setString(3,event.msg.sender.trim());
	        		ps_addEmailType.setString(4,event.msg.template);
	        		ps_addEmailType.setBigDecimal(5,bd_currentTS);
	        		ps_addEmailType.execute();
	        		uuid=temp_uuid;
	        	}
	        }
	        finally
	        {
	        	if(ps!=null)
	        		{
	        			ps.close();
	        		}
	        	if(rs!=null)
	        	{
	        		rs.close();
	        	}
	        	return uuid;
	        }
	        }
	        
	        public static void addSendEvent(String emailUUID,Event event,Connection conn) throws SQLException
	        {
	        	BigDecimal bd_currentTS=new BigDecimal(System.currentTimeMillis());
	        	
	        	PreparedStatement ps=null;
	        try
	        {
	        	if(event.msg.tags==null)
	        	{
	        		event.msg.tags=new String[5];
	        	}
	        	else if(event.msg.tags!=null)
	        	{
	        		String tempTags[]=new String[5];
	        		int i=0;
	        		for(i=0;i<5;i++)
	        		{
	        			if(i<event.msg.tags.length) //to copy tag from event received to tempTags
	        				{tempTags[i]=event.msg.tags[i];}
	        			//else it will remain null automatically
	        			
	        		}
	        		System.out.println("i and tags.length:"+i+" "+event.msg.tags.length);
	        		if(i==5 && event.msg.tags.length>5) //will enter only if more than 5 tags exist
	        		{
	        			
	        			tempTags[4]="".trim();
	        			//start from 5th tag received(index 4)
	        			for(i=4;i<event.msg.tags.length;i++)
	        			{ 
	        				//copy all tags from index 4 onwards and join them into 1 string
	        				if(i!=4)
	        				{
	        				tempTags[4]=tempTags[4]+","+event.msg.tags[i];
	        				}
	        				else
	        				{
	        					tempTags[4]=event.msg.tags[i];
	        				}
	        				
	        			}
	        			
	        		}
	        		event.msg.tags=tempTags;
	        	}
	        	 ps=conn.prepareStatement("insert into "+DBConfiguration.emailInstanceTable+" values(?,?,?,?,?,?,?,?,?,?,?,?)");
	        	ps.setString(1,emailUUID);//FK for emailType table
	        	ps.setString(2,UUIDGenerator.getUUID()); //event's uuid
	        	ps.setString(3,event.msg.email);//recepient
	        	ps.setString(4,event.msg.ts);
	        	ps.setString(5,event.msg._id);
	        	ps.setString(6,event.msg.state);
	        	ps.setBigDecimal(7,bd_currentTS);
	        	
	        	ps.setString(8,event.msg.tags[0]);
	        	ps.setString(9,event.msg.tags[1]);
	        	ps.setString(10,event.msg.tags[2]);
	        	ps.setString(11,event.msg.tags[3]);
	        	ps.setString(12,event.msg.tags[4]);
		        
	        	
	        	ps.execute();
	        	
	        }
	        	  finally
	  	        {
	  	        	if(ps!=null)
	  	        		{
	  	        			ps.close();
	  	        		}
	  	        }	
	  	      
	        	
	        }
	        public static void addOpenEvent(String emailUUID,Event event,Connection conn) throws SQLException
	        {
	        	BigDecimal bd_currentTS=new BigDecimal(System.currentTimeMillis());
	        	PreparedStatement ps=null;
	        	String formatted_msg_ts=null;
	        	DateTime msg_ts_date=null;
	        	try
	        	{
	        	
	        		if(event.msg.smtp_events==null)
		        	{ 
		        		event.msg.smtp_events=new Smtp_Events[1];
		        		event.msg.smtp_events[0]=new Smtp_Events(); 
		        	}
		        	
		        	else if(event.msg.smtp_events.length==0)
		        	{
		        		event.msg.smtp_events=new Smtp_Events[1];
		        		event.msg.smtp_events[0]=new Smtp_Events(); 
		        		
		        	}
	        		if(event.msg.ts!=null)
	        		{
	        			DateTimeFormatter dtf = DateTimeFormat.forPattern("MM-dd-yyyy HH:mm:ss");
	        			msg_ts_date= new DateTime(Long.parseLong(event.msg.ts) * 1000L);
	        		
	        			formatted_msg_ts=dtf.print(msg_ts_date);
	        		}
	        		//if user agent parsed is null
	        		//e.g."user_agent":"Microsoft Office\/16.0 (Microsoft Outlook Mail 16.0.7466; Pro)","user_agent_parsed":null
	        		if(event.user_agent_parsed==null && event.user_agent==null)
	        		{
	        			event.user_agent_parsed=new User_Agent_Parsed();
	        		}
	        		else if(event.user_agent_parsed==null && event.user_agent!=null)
	        		{
	        			event.user_agent_parsed=new User_Agent_Parsed();
	        			event.user_agent_parsed.ua_name=event.user_agent;
	        		}
	        		
	        		//if location is null this avoids NPE
	        		if(event.location==null)
	        		{
	        			event.location=new MandrillReport.Location();
	        		}
	        			
	        		
		        	ps=conn.prepareStatement("insert into "+DBConfiguration.openTable+"(emailUUID,eventUUID,ip,url,msg_email,ua_mobile,ua_os_company,ua_os_name,ua_family,ua_name,ua_type,loc_country_short,loc_country_long,loc_region,loc_city,loc_postal_code,loc_timezone,loc_latitude,loc_longitude,smtp_destination_ip,smtp_source_ip,smtp_type,smtp_diag,entryTS,msg_ts,formatted_msg_ts) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		        	ps.setString(1,emailUUID);//FK for emailType table
		        	ps.setString(2,UUIDGenerator.getUUID()); //event's uuid
		        	ps.setString(3,event.ip);
		        	ps.setString(4,event.url);
		        	ps.setString(5,event.msg.email); //recepient
		        	ps.setString(6,event.user_agent_parsed.mobile);
		        	ps.setString(7,event.user_agent_parsed.os_company);
		        	ps.setString(8,event.user_agent_parsed.os_name);
		        	ps.setString(9,event.user_agent_parsed.ua_family);
		        	ps.setString(10,event.user_agent_parsed.ua_name);
		        	ps.setString(11,event.user_agent_parsed.type);
		        	ps.setString(12,event.location.country_short);
		        	ps.setString(13,event.location.country_long);
		        	ps.setString(14,event.location.region);
		        	ps.setString(15,event.location.city);
		        	ps.setString(16,event.location.postal_code);
		        	ps.setString(17,event.location.timezone);
		        	ps.setString(18,event.location.latitude);
		        	ps.setString(19,event.location.longitude);
		        	ps.setString(20,event.msg.smtp_events[0].destination_ip);
		        	ps.setString(21,event.msg.smtp_events[0].source_ip);
		        	ps.setString(22,event.msg.smtp_events[0].type);
		        	ps.setString(23,event.msg.smtp_events[0].diag);
		        	ps.setBigDecimal(24,bd_currentTS);
		        	ps.setString(25,event.msg.ts);
		        	ps.setString(26,formatted_msg_ts);
		        	ps.execute();
	        	}
	        	finally
	        	{
	        		if(ps!=null)
	        		{
	        			ps.close();
	        		}
	        	}
	        	
	        	
	        	
	        }
	        public static void addClickEvent(String emailUUID,Event event,Connection conn) throws SQLException
	        {
	        	BigDecimal bd_currentTS=new BigDecimal(System.currentTimeMillis());
	        	PreparedStatement ps=null;
	        	String formatted_msg_ts=null;
	        	DateTime msg_ts_date=null;
	        	
	        	try
	        	{
		        	if(event.msg.smtp_events==null)
		        	{ 
		        		event.msg.smtp_events=new Smtp_Events[1];
		        		event.msg.smtp_events[0]=new Smtp_Events(); 
		        	}
		        	
		        	else if(event.msg.smtp_events.length==0)
		        	{
		        		event.msg.smtp_events=new Smtp_Events[1];
		        		event.msg.smtp_events[0]=new Smtp_Events(); 
		        		
		        	}
		        	//get actual date 
		        	
		    		if(event.msg.ts!=null)
	        		{
		    			DateTimeFormatter dtf = DateTimeFormat.forPattern("MM-dd-yyyy HH:mm:ss");
	        			msg_ts_date= new DateTime(Long.parseLong(event.msg.ts) * 1000L);
	        		
	        			formatted_msg_ts=dtf.print(msg_ts_date);
	        			
		    			//DateTimeFormatter dtf = DateTimeFormat.forPattern("MM-dd-yyyy HH:mm:ss");
	        			//msg_ts_date= dtf.parseDateTime((new DateTime(Long.parseLong(event.msg.ts) * 1000L)).toString());
	        		
	        			//		formatted_msg_ts=msg_ts_date.toString();
	        		}
		    
		    		if(event.user_agent_parsed==null && event.user_agent==null)
	        		{
	        			event.user_agent_parsed=new User_Agent_Parsed();
	        		}
	        		else if(event.user_agent_parsed==null && event.user_agent!=null)
	        		{
	        			event.user_agent_parsed=new User_Agent_Parsed();
	        			event.user_agent_parsed.ua_name=event.user_agent;
	        		}
		    		
		    		//if location is null this avoids NPE
	        		if(event.location==null)
	        		{
	        			event.location=new MandrillReport.Location();
	        		}
		        	ps=conn.prepareStatement("insert into "+DBConfiguration.clickTable+"(emailUUID,eventUUID,ip,url,msg_email,ua_mobile,ua_os_company,ua_os_name,ua_family,ua_name,ua_type,loc_country_short,loc_country_long,loc_region,loc_city,loc_postal_code,loc_timezone,loc_latitude,loc_longitude,smtp_destination_ip,smtp_source_ip,smtp_type,smtp_diag,entryTS,msg_ts,formatted_msg_ts) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		        	ps.setString(1,emailUUID);//FK for emailType table
		        	ps.setString(2,UUIDGenerator.getUUID()); //event's uuid
		        	ps.setString(3,event.ip);
		        	ps.setString(4,event.url);
		        	ps.setString(5,event.msg.email); //recepient
		        	ps.setString(6,event.user_agent_parsed.mobile);
		        	ps.setString(7,event.user_agent_parsed.os_company);
		        	ps.setString(8,event.user_agent_parsed.os_name);
		        	ps.setString(9,event.user_agent_parsed.ua_family);
		        	ps.setString(10,event.user_agent_parsed.ua_name);
		        	ps.setString(11,event.user_agent_parsed.type);
		        	ps.setString(12,event.location.country_short);
		        	ps.setString(13,event.location.country_long);
		        	ps.setString(14,event.location.region);
		        	ps.setString(15,event.location.city);
		        	ps.setString(16,event.location.postal_code);
		        	ps.setString(17,event.location.timezone);
		        	ps.setString(18,event.location.latitude);
		        	ps.setString(19,event.location.longitude);
		        	ps.setString(20,event.msg.smtp_events[0].destination_ip);
		        	ps.setString(21,event.msg.smtp_events[0].source_ip);
		        	ps.setString(22,event.msg.smtp_events[0].type);
		        	ps.setString(23,event.msg.smtp_events[0].diag);
		        	ps.setBigDecimal(24,bd_currentTS);
		        	ps.setString(25,event.msg.ts);
		        	ps.setString(26,formatted_msg_ts);
			        
		        	ps.execute();
	        	}
	        	finally
	        	{
	        		if(ps!=null)
	        		{
	        			ps.close();
	        		}
	        	}
		    }
	        
	        public static void addOtherEvent(String emailUUID,Event event,Connection conn) throws SQLException
	        {
	        	BigDecimal bd_currentTS=new BigDecimal(System.currentTimeMillis());
	        	PreparedStatement ps=null;
	        	try
	        	{
		        	if(event.msg==null)
		        	{
		        		event.msg=new Message();
		        	}
		        	
		        	if(event.msg.smtp_events==null)
		        	{ 
		        		event.msg.smtp_events=new Smtp_Events[1];
		        		event.msg.smtp_events[0]=new Smtp_Events(); 
		        	}
		        	
		        	else if(event.msg.smtp_events.length==0)
		        	{
		        		event.msg.smtp_events=new Smtp_Events[1];
		        		event.msg.smtp_events[0]=new Smtp_Events(); 
		        		
		        	}
		           ps=conn.prepareStatement("insert into "+DBConfiguration.otherEventsTable+" values(?,?,?,?,?,?,?,?,?,?,?)");
		        	ps.setString(1,emailUUID);//FK for emailType table
		        	ps.setString(2,UUIDGenerator.getUUID()); //event's uuid
		        	
		        	ps.setString(3,event.msg.smtp_events[0].destination_ip);
		        	ps.setString(4,event.msg.smtp_events[0].source_ip);
		        	ps.setString(5,event.msg.smtp_events[0].type);
		        	ps.setString(6,event.msg.smtp_events[0].diag);
		        	ps.setString(7,event.msg.diag);
		        	ps.setString(8, event.msg.bounce_description);
		        	ps.setString(9,event.msg._id);
		        	ps.setString(10,event.msg.email);
		        	ps.setBigDecimal(11,bd_currentTS);
		        	ps.execute();
	        	}
	        	finally
	        	{
	        		if(ps!=null)
	        		{
	        			ps.close();
	        		}
	        	}
	        	 
	        }
	        
	        public static void addSyncEvent(SyncEvents event)
	        {
	        	
	        	System.out.println("ADD to DB:...Sync Event ts:"+event.ts);
				System.out.println("Sync Event type:"+event.type);
				System.out.println("Sync Event Reject.email:"+event.reject.email);
				PreparedStatement ps=null;
				Connection conn=null;
	    		 try 
	    		 {
					
	    			 Class.forName(DBConfiguration.driver);
				} 
	    		 catch (ClassNotFoundException e) 
	    		 {
					// TODO Auto-generated catch block
					e.printStackTrace();
					 MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
				}
	 	        //connect to DB
	            
				try {
					conn = DriverManager.getConnection(DBConfiguration.url,DBConfiguration.username,DBConfiguration.password);
				                
	            
					ps = conn.prepareStatement("insert into "+DBConfiguration.syncEventsTable+" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		           
					 if(event.reject.detail!=null)
			            {
			            	if(event.reject.detail.length()>=50)
			            	{
			            		event.reject.detail=event.reject.detail.substring(0,49);
			            	}
			            }
					 if(event.reject.reason!=null)
			            {
			            	if(event.reject.reason.length()>=50)
			            	{
			            		event.reject.reason=event.reject.reason.substring(0,49);
			            	}
			            }
					
					ps.setString(1,UUIDGenerator.getUUID());
		            BigDecimal bd_receivedOn=new BigDecimal(System.currentTimeMillis());
		            ps.setBigDecimal(2, bd_receivedOn);
		            ps.setBigDecimal(3,new BigDecimal(event.ts));
		           
		            ps.setString(4,event.type);
		            ps.setString(5,event.action );
		            ps.setString(6,event.reject.reason);
		           
		            ps.setString(7, event.reject.detail);
		            ps.setString(8, event.reject.last_event_at);
		            ps.setString(9, event.reject.email);
		            ps.setString(10, event.reject.created_at);
		            ps.setString(11, event.reject.expires_at);
		            ps.setString(12,event.reject.expired);
		            ps.setString(13,event.reject.subaccount);
		            ps.setString(14, event.reject.sender);
		            ps.execute();
				}
				catch(Exception e)
				{
					System.out.println(e);
					e.printStackTrace();
					 MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
				}
				finally
				{
					if(ps!=null)
					{
						try 
						{
							ps.close();
						}
						catch (SQLException e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
							MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
						}
						
						
						if(conn!=null)
						{
							try {
								conn.close();
							} 
							catch (SQLException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
								MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
							}
						}
					}
				}
	        }
	

	    
//this method no longer used//
/*
	public static void handleRanexaEvents(Event event) throws SQLException
	{
		Connection ranexaConn=null;
		String fulfillment_id=null;
		
		String email_addr=null;
		String person_id=null;
		String comm_rule_id=null;
		Integer maxLogDetail_id=null;
		Integer maxLaunchID=null;
		String launch_id=null;
		PreparedStatement ps=null;
		PreparedStatement ps_maxLaunchID=null;
		PreparedStatement ps_maxLogDetail=null;
		PreparedStatement ps_responsys=null;
		ResultSet  rs_maxLaunchID=null;
		ResultSet rs_maxLogDetail=null;
		ResultSet rs=null;
		java.util.List<String> receivedTags=Arrays.asList(ArrayUtils.nullToEmpty(event.msg.tags));
		
		 try {
				Class.forName(DBConfiguration.driver);
				
				
				
				if(event.msg!=null && event.msg.smtp_events==null)
				{
					event.msg.smtp_events=new Smtp_Events[1];
					event.msg.smtp_events[0]=new Smtp_Events();
				
				}
				
				else if (event.msg.smtp_events!=null && event.msg.smtp_events.length==0)
				{
					event.msg.smtp_events=new Smtp_Events[1];
					event.msg.smtp_events[0]=new Smtp_Events();
				}
			} 
		 catch (Exception e) 
		 {
				// TODO Auto-generated catch block
				e.printStackTrace();
				 MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
			}
	 	try
	 	{
			 
		//return if event is null
			if(event.event==null)
			{
				return;
			}
			
			if(event.msg!=null && event.msg.smtp_events==null)
			{
				//for later use in open/click events
				event.msg.smtp_events=new Smtp_Events[1];
				event.msg.smtp_events[0]=new Smtp_Events();
			}
			if(event.msg!=null && event.msg.tags!=null)
			{
				if(event.msg.tags.length>=2)
				{
					
					if(receivedTags.contains(DBConfiguration.ranexaTag.trim()))
							{
									System.out.println("ranexa event received...");
									fulfillment_id=event.msg.tags[1];
										for(int tagindex=0;tagindex<event.msg.tags.length;tagindex++)
										{
											System.out.println("tag index:"+tagindex);
											String tag=event.msg.tags[tagindex];
											if(tag==null)
											{
												tag="";
											}
											if(tag.startsWith("FID"))
											{
												fulfillment_id=tag;
												
											}
										}
										//fulfillment id tag received. Extract the id
									if(fulfillment_id.length()>4)
									{
										fulfillment_id=fulfillment_id.substring(4);
									
									}
									else
									{
										fulfillment_id=fulfillment_id.replace("FID_","".trim());
										MandrillReport.mandrillReportLogger.log(Level.SEVERE,"Ranexa tag received but fulfilment id "+ fulfillment_id+" length<=4 and ID cannot be extracted");
									System.out.println("ranexa tag received.Cannot extract FID");
									}
								System.out.println("fulfillment id received:"+fulfillment_id);
								//launch_id=event.msg.tags[0];
							}
					else
					{
						System.out.println("not a ranexa event..return");
						return;
					}
					
					
				}
				else 
				{
					System.out.println("less than 2 tags..cannot be a ranexa event..return");
					return;
				}
					
			}
			else
			{
				System.out.println("ranexa msg tags null..returning");
				return;
			}
			
			
			ranexaConn = DriverManager.getConnection(DBConfiguration.ranexaUrl+";user="+DBConfiguration.ranexaUsername+";password="+DBConfiguration.ranexaPassword);
			

			//GET MAX LAUNCH ID
			ps_maxLaunchID=ranexaConn.prepareStatement("select max(launch_id) from "+DBConfiguration.ranexaReponsysTable);
			ps_maxLaunchID.executeQuery();
			rs_maxLaunchID=ps_maxLaunchID.executeQuery();
			if(rs_maxLaunchID.next())
			{
				maxLaunchID=rs_maxLaunchID.getInt(1);
				if(maxLaunchID!=null)
					{
						launch_id=Integer.toString(maxLaunchID+1);
						System.out.println("launch id incremented to:"+launch_id);
					}
			}
			
			//GET INFO BASED ON FULFILLMENT ID
			 ps=ranexaConn.prepareStatement("select person_id,email_addr,comm_rule_id from "+DBConfiguration.ranexaFulfillmentTable+" where fulfillment_id=?");
			ps.setString(1,fulfillment_id);
			rs=ps.executeQuery();
			
			
			if(rs.next())
			{
				person_id=rs.getString("person_id");
				comm_rule_id=rs.getString("comm_rule_id");
				email_addr=rs.getString("email_addr");
			}
			else
			{
				System.out.println("info not found in fulfilment table");
				MandrillReport.mandrillReportLogger.log(Level.SEVERE, "information not found in fulfillment table for FID:"+fulfillment_id);
			}
			
			
				if(event.msg!=null && event.msg.email!=null)
				{
					email_addr=event.msg.email;
				}
			
			
			//get max log detail id
			 ps_maxLogDetail=ranexaConn.prepareStatement("select max(log_detail_id) from "+DBConfiguration.ranexaReponsysTable);
			ps_maxLogDetail.executeQuery();
			rs_maxLogDetail=ps_maxLogDetail.executeQuery();
			if(rs_maxLogDetail.next())
			{
				maxLogDetail_id=rs_maxLogDetail.getInt(1);
			}
			
			//add info to responsys table
			ps_responsys=ranexaConn.prepareStatement("insert into "+DBConfiguration.ranexaReponsysTable+"(log_detail_id,email_addr,action,description,event_tz,launch_id,comm_rule_id,person_id,fulfillment_id,status,diagnostic_cd,insert_tz,linked_tx,processed_fl) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			if(maxLogDetail_id==null)
			{
				maxLogDetail_id=1;
			}
			ps_responsys.setInt(1,maxLogDetail_id+1);
			ps_responsys.setString(2,email_addr);
			ps_responsys.setString(3,formatEventForRanexa(event.event));
			
			ps_responsys.setString(4,"H");
			ps_responsys.setTimestamp(5,new Timestamp(Long.parseLong(event.ts)*1000));
			ps_responsys.setString(6,launch_id);
			ps_responsys.setString(7,comm_rule_id);
			ps_responsys.setString(8,person_id);
			ps_responsys.setString(9,fulfillment_id);
			ps_responsys.setString(10, event.msg.state);
			ps_responsys.setString(11, event.msg.diag+":"+event.msg.bounce_description+":"+event.msg.smtp_events[0].diag);
			ps_responsys.setTimestamp(12,new Timestamp(System.currentTimeMillis()));
			ps_responsys.setString(13,event.url);
			ps_responsys.setString(14,"N");

			ps_responsys.execute();
			System.out.println("data added to responsys");
			
			
	  }
	 	catch(Exception e)
	 	{
	 		e.printStackTrace();
	 		 MandrillReport.mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
	 	}
	 	finally
	 	{
	 		if(ps!=null)
	 		{
	 			ps.close();
	 		}
	 		
	 		if(rs!=null)
	 		{
	 			rs.close();
	 		}
	 		if(ps_maxLogDetail!=null)
	 		{
	 			ps_maxLogDetail.close();
	 		}
	 		if(rs_maxLogDetail!=null)
	 		{
	 			rs_maxLogDetail.close();
	 		}
	 		if(ps_responsys!=null)
	 		{
	 			ps.close();
	 		}
	 		if(ranexaConn!=null)
	 		{
	 				ranexaConn.close();
			
	 		}
	 		
	 	}

	}
	
	public static String formatEventForRanexa(String event)
	{
		String returnEvent=null;
		if(event==null)
		{
			return null;
		}
		if(event.equals("send"))
		{
			returnEvent="_Sent_";
		}
		
		else if(event.equals("soft_bounce"))
		{
			returnEvent="_Bounced_";
		}
		else if(event.equals("hard_bounce"))
		{
			returnEvent="_Bounced_";
		}
		else if(event.equals("open"))
		{
			returnEvent="_Opened_";
		}
		else if(event.equals("click"))
		{
			returnEvent="_Clicked_";
		}
		
		else if(event.equals("spam"))
		{
			returnEvent="_Complaint_";
		}
		else if(event.equals("reject"))
		{
			returnEvent="_Skipped_";
		}
		
		
		
		return returnEvent;
	}*/
}