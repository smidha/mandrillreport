package com.arches.utilities;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;


import com.google.gson.Gson;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.controller.MandrillTemplatesApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import com.microtripit.mandrillapp.lutung.view.MandrillTemplate;
import com.microtripit.mandrillapp.lutung.view.MandrillUserInfo;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;

public class EmailGeneratorTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	
    	String REPROFILEURL="";
    	String SIGNINURL="";
    	String UNSUBSCRIBEURL="";
    	
    	
    	 MandrillApi mandrillApi=new MandrillApi("vqMxkNfpRH5L6Lu0tyaf_Q");
    	 MandrillTemplatesApi mandrilltemplatesapi = mandrillApi.templates();               
         MandrillTemplate MandrillTemplate = null;
		try {
			MandrillTemplate = mandrilltemplatesapi.info("Cayston_WelcomeEmail");
		} catch (MandrillApiError e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
        String emailContent = MandrillTemplate.getPublishCode();
         
         // create your message
              MandrillMessage message = new MandrillMessage();
              message.setTrackClicks(Boolean.TRUE);
              message.setTrackOpens(Boolean.TRUE);
              
              List<MergeVar> globalMergeVars = new ArrayList<MergeVar>();
              MergeVar objVar = new MergeVar();
              objVar.setName("FNAME");
              objVar.setContent("demo");      
              
              MergeVar objVar1 = new MergeVar();
              objVar1.setName("UUID");
              objVar1.setContent("");
              
              MergeVar objVar2 = new MergeVar();
              objVar2.setName("REPROFILEURL");
              objVar2.setContent(REPROFILEURL);
              
              MergeVar objVar3 = new MergeVar();
              objVar3.setName("SIGNINURL");
              objVar3.setContent(SIGNINURL);
              
              MergeVar objVar4 = new MergeVar();
              objVar4.setName("UNSUBSCRIBEURL");
              objVar4.setContent(UNSUBSCRIBEURL);
              
              globalMergeVars.add(objVar);
              globalMergeVars.add(objVar1);
              globalMergeVars.add(objVar2);
              globalMergeVars.add(objVar3);
              globalMergeVars.add(objVar4);
              
              
              message.setGlobalMergeVars(globalMergeVars);
             
             
    	  
    	  
          message.setAutoText(true);
          message.setFromEmail(MandrillTemplate.getFromEmail());
          message.setFromName(MandrillTemplate.getFromName());
          

           List<Recipient> recipients = new ArrayList<Recipient>();  
          	Recipient recipient = new Recipient();
          	recipient.setEmail("smidha@archestechnology.com");
          	recipient.setName("yv");
          	recipients.add(recipient);
          
          message.setTo(recipients);
          message.setPreserveRecipients(true);
          try {
        //      send the email
        	  java.util.Date date = new java.util.Date();
              String sendAt= new SimpleDateFormat("yyyy-MM-dd").format(date);  String hhmmss = getCurrentTimeinHHMMSS();
              
              sendAt = sendAt + " " + hhmmss;
              java.util.Date sendDate = getSendDate(sendAt);
              MandrillMessageStatus[] messageStatusReports = mandrillApi.messages().sendTemplate("Cayston_WelcomeEmail", null, message, Boolean.TRUE,"",sendDate);
        
        	  System.out.println("~~~~~~~sending welcome email to:"+"yverma");
            for(int j=0;j<messageStatusReports.length;j++)
           {
              System.out.println("Email Sent Status:"+messageStatusReports[j].getStatus());
            } 
          } catch (MandrillApiError ex) {
          System.out.println("mandrill exception."+ex);
          } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}

	}
public static java.util.Date getSendDate(String send_at)throws Exception{
        
        String string = send_at;
         DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         java.util.Date sendDate = format.parse(string);
         return sendDate;
    }
      
      public static String getCurrentTimeinHHMMSS()
      {
         SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
         java.util.Date now = new java.util.Date();
         String strDate = sdfDate.format(now);
         System.out.println(strDate);
         
         return strDate;
      
      }
    

}
