package com.arches.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import com.google.gson.Gson;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import com.microtripit.mandrillapp.lutung.view.MandrillUserInfo;

public class EmailGenerator
{
    private final String mandrillFile;
    private final String senderEmail;
    private final String apiKey;
    private final String senderName;
    private final String[] recipientsEmails;
    private final String[] recipientsNames;
//constructor
    public EmailGenerator(String mandrillFile)
    {
            this.mandrillFile=mandrillFile; //initialize mandrillFile location
            ResourceBundle mandrillResource = ResourceBundle.getBundle(mandrillFile);
            this.senderEmail=mandrillResource.getString("senderEmail");
            this.senderName=mandrillResource.getString("senderName");
            this.apiKey=mandrillResource.getString("KEY");
            this.recipientsEmails=mandrillResource.getString("recipientsEmails").split(",");
            this.recipientsNames=mandrillResource.getString("recipientsNames").split(",");
            
    }
public static void main(String[] args)
{
	EmailGenerator eg=new EmailGenerator("MandrillConfig");
	try {
		eg.generateEmail();
	} catch (MandrillApiError e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("api errorrrrrr");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("io exception!!!!!");
	}
}
    
    //method to generate reminder email
    public void generateEmail() throws MandrillApiError, IOException
    {
    	 //call method to generate email
     
        //MandrillConfiguration gets the mandrill account properties
        MandrillApi mandrillApi=new MandrillApi(this.apiKey);
        MandrillUserInfo user = mandrillApi.users().info();
     
        System.out.println( new Gson().toJson(user) );

       //build email message and add sender information
        MandrillMessage message=new MandrillMessage();
        message.setSubject("test email for mandrill ranexa report ");
        
        message.setHtml("test ranexa events:");
       
         message.setAutoText(true);
        message.setFromEmail(this.senderEmail);
        message.setFromName(this.senderName);
        

         List<Recipient> recipients = new ArrayList<Recipient>();  
        for(int i=0;i<recipientsEmails.length;i++)
        {       
        	Recipient recipient = new Recipient();
        	recipient.setEmail(recipientsEmails[i]);
        	recipient.setName(recipientsNames[i]);
        		recipients.add(recipient);
        }
        message.setTo(recipients);
        message.setPreserveRecipients(true);
        message.setTrackClicks(Boolean.TRUE);
        message.setTrackOpens(Boolean.TRUE);
      List<String> tags=new ArrayList<String>();
      tags.add("MyRanexaConnect");
     
        
      tags.add("PID_1500001");
      tags.add("CID_46000");
      tags.add("HID_http://html");
        tags.add("FID_");   
        message.setTags(tags);
        
     
        
        
        try {
      //      send the email
      
          MandrillMessageStatus[] messageStatusReports = mandrillApi.messages().send(message, false);
          for(int j=0;j<messageStatusReports.length;j++)
         {
            System.out.println("Email Sent Status:"+messageStatusReports[j].getStatus());
          } 
        } catch (MandrillApiError ex) {
        System.out.println("mandrill exception."+ex);
        }
    }
    
}