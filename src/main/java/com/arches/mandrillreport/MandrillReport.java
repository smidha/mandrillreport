package com.arches.mandrillreport;

import java.awt.Event;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.omg.PortableInterceptor.LOCATION_FORWARD;

import com.arches.utilities.DBConfiguration;



public class MandrillReport {

	private static final String EVENTS_KEY;
	private static final String KEY;
	private static final String AUTH_KEY;
	private static final String SERVLET_URL;
	private static final String filterEmailsFlag;
	private static final List<String> filterSenderAddresses;
	public static Logger mandrillReportLogger;
	public static String mandrillReportLogsLocation;
	public static ObjectMapper mapper;

static
	{
	ResourceBundle rb= ResourceBundle.getBundle("MandrillConfig");
		EVENTS_KEY=rb.getString("EVENTS_KEY");
		KEY=rb.getString("KEY");
		AUTH_KEY =rb.getString("AUTH_KEY");
		SERVLET_URL=rb.getString("SERVLET_URL");
		filterEmailsFlag=rb.getString("filterEmailsFlag");
		filterSenderAddresses=Arrays.asList(org.apache.commons.lang3.ArrayUtils.nullToEmpty(rb.getString("filterSenderAddresses").split(",")));
		mandrillReportLogger=Logger.getLogger("mandrillReportLogger");
		mandrillReportLogsLocation=rb.getString("mandrillReportLogsLocation")+"MandrillReportLogs_";
		  mapper=new ObjectMapper();
	}
	

	public boolean receiveReport(HttpServletRequest request,HttpServletResponse response)
	{
		boolean result=true;
		FileHandler fileHandler;
	
		
		try
		{
	  		try
	  		{
	  			
	  			fileHandler=new FileHandler(mandrillReportLogsLocation+Calendar.getInstance().get(Calendar.DAY_OF_MONTH)+"_"+Calendar.getInstance().get(Calendar.MONTH)+"_"+Calendar.getInstance().get(Calendar.YEAR)+".txt",true);
	  			mandrillReportLogger.addHandler(fileHandler);
	  			SimpleFormatter simpleFormatter=new SimpleFormatter();
	  			fileHandler.setFormatter(simpleFormatter);
	  			mandrillReportLogger.log(Level.INFO,"Logging for date:"+Calendar.getInstance().getTime(),"Logging for date:"+Calendar.getInstance().getTime());
	  		}
	  		catch(Exception e)
	  		{
	  			e.printStackTrace();
	  			result=false;
	  		}
		
		String signature = request.getHeader("X-Mandrill-Signature");
		if (signature == null) {
			System.out.println("Mandrill request without signature.Cannot proceed ahead with DB entry");
			//do not proceed ahead
			result=false;
			return false;
		}
		
		
		StringBuilder data = new StringBuilder();
		
		String rawEvents = (String) request.getParameter(EVENTS_KEY);
		if (rawEvents == null) {
			System.out.println("No events provided. No DB entry required");
			//do not proceed ahead
			result=false;
			return false;
		}
			System.out.println("Data Received.Printing event(s):"+rawEvents.toString());
		
	//	data.append(SERVLET_URL.trim());
	//	data.append(EVENTS_KEY.trim());
	//	data.append(rawEvents.trim());
		/*SKIP SIGNATURE CHECK
		System.out.println("data string formed:"+data);
		
		String calculatedSignature = EncryptionUtil.calculateRFC2104HMACInBase64(data.toString().trim(),AUTH_KEY);
		
		System.out.println("calculated sign:"+calculatedSignature+" and received sign:"+signature);
		
		

		if (!signature.equals(calculatedSignature)) {
		System.out.println("Signatures do not match. Cannot proceed with DB entry of event");
		//do not procced ahead
		result=false;
		return false;
		}
		System.out.println("Signatures match. Proceeding ahead");
		//signatures match. proceeding ahead
		*/
	
	List<Event> events = null;
	
	
	System.out.println("events extraction..");
	
	mapper.configure(org.codehaus.jackson.map.DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	
	try 
	{
			System.out.println("events extraction start..");
			events = mapper.readValue(rawEvents, new TypeReference<List<Event>>() {
		});
			System.out.println("events extraction step complete..");
	} 
	catch (IOException e) {
		
		System.out.println("Invalid event format. Exception:"+e);
		e.printStackTrace();
		mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
		//do not proceed ahead
		result=false;
		return false;
		
	}

	//app is on but won't log any data
	if(filterEmailsFlag.equals("false"))
	{
		//do not proceed further for processing.return true 
		System.out.println("Filter is set to false. Event Received. Not logging any data into DB. ");
		//do not proceed ahead
		result=true;
		return true;
	}
	
	//filerEmail flags=all -> log ALL events. filterEmail flag=true -> log events for only some email ids
	System.out.println("configuring db");
	DBConfiguration.configureDB("DBConfiguration");
	
	
	for (Event event : events) {
		if(event.event==null)
		{
			if(event.type!=null)
			{
				System.out.println("event of type:"+event.type);
				//handle sync events
				handleSyncEvent(rawEvents);
			}
			continue;
		}
		
		
		
		if(filterEmailsFlag.equals("true") && !filterSenderAddresses.contains(event.msg.sender))
		{
			System.out.println("Filtering Addresses:The sender is not present in list of addresses to be filtered. No DB Entry");
			continue;
		}
		System.out.println("Event: " + event.event);
		System.out.println("Receiver Email: " + event.msg.email);
		System.out.println("Msg sender: " + event.msg.sender);
		//add to db
		
		
		DBConfiguration.addEvents(event);
		
		}
		}
		catch(Exception e)
		{
			mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
			e.printStackTrace();
			result=false;
			return  false;
		}
		finally
		{
			for(Handler h:mandrillReportLogger.getHandlers())
			{
			    h.close();   //must call h.close or a .LCK file will remain.
			}
			return result;
		}
	}
	
	public void handleSyncEvent(String rawEvent)
	{
		List<SyncEvents> events = null;
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(org.codehaus.jackson.map.DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try 
		{
				events = mapper.readValue(rawEvent, new TypeReference<List<SyncEvents>>() {
			});
		} 
		catch (IOException e) {
			
			System.out.println("Invalid event format. Exception:"+e);
			mandrillReportLogger.log(Level.SEVERE,e.getMessage(),e);
			
		}
			for (SyncEvents event : events)
			{
				if(event.type==null)
				{
					continue;
				}
				if(filterEmailsFlag.equals("true") && !filterSenderAddresses.contains(event.reject.sender))
				{
					System.out.println("Filtering Addresses:The sender is not present in list of addresses to be filtered. No DB Entry");
					continue;
				}
				DBConfiguration.addSyncEvent(event);
				System.out.println("Sync Event ts:"+event.ts);
				System.out.println("Sync Event type:"+event.type);
				System.out.println("Sync Event action:"+event.action);
				System.out.println("Sync Event Reject.reason"+event.reject.reason);
				System.out.println("Sync Event Reject.detail:"+event.reject.detail);
				System.out.println("Sync Event Reject.email:"+event.reject.email);
				System.out.println("Sync Event Reject.createdat:"+event.reject.created_at);
				System.out.println("Sync Event Reject.expired:"+event.reject.expired);
				System.out.println("Sync Event Reject.expires_at:"+event.reject.expires_at);
				System.out.println("Sync Event Reject.subaccount:"+event.reject.subaccount);
				System.out.println("Sync Event Reject.sender:"+event.reject.sender);
			}
		}
		

	


	public static class Event {
	
		
		public String event;
		public Message msg;
		public String ts;
		public String user_agent; //for open/click event
		public User_Agent_Parsed user_agent_parsed; //for open/click event
		public String url; // for click event
		public Location location;
		public String _id;	
		//for sync event checking
		public String type;
		public String ip;
	}

	public static class Message {
		public String _id;
			public String email;
		//public String opens;
		//public String reject;
		//public String resends;
		public String tags[];
		public String sender;
		//public String smtp_events;
		public String state;
		//public String subaccount;
		public String subject;
		public Smtp_Events smtp_events[];
		
		public String template;
		public String ts;
		public String metadata;
		public String subaccount;
		public String diag;
		public String bounce_description;
		
	}
	
	public static class Smtp_Events
	{
		public String ts;
		public String type;
		public String diag;
		public String source_ip;
		public String destination_ip;
		public String size;
	}
	
	public static class Location
	{
		public String country_short;
		public String country_long;
		public String region;
		public String city;
		public String postal_code;
		public String timezone;
		public String latitude;
		public String longitude;
	}
	
	public static class User_Agent_Parsed
	{
		public String mobile;
		public String os_company;
		public String os_family;
		public String os_name;
		public String type; //mobile browser etc
		public String ua_family;
		public String ua_version;
		public String ua_name;
	}
	
	public static class SyncEvents
	{
		public String ts;
		public String type;
		public String action;
		public Reject reject;
	}
	public static class Reject
	{
		public String reason;
		public String detail;
		public String last_event_at;
		public String email;
		public String created_at;
		public String expires_at;
		public String expired;
		public String subaccount;
		public String sender;
	}
}